--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

-- Started on 2019-01-25 20:50:51 -02

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 13043)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2994 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 204 (class 1259 OID 16663)
-- Name: aluno; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aluno (
    matricula_aluno character varying(20) NOT NULL,
    id_aluno character varying(20) NOT NULL,
    cr character varying(20) NOT NULL,
    nome character(40) NOT NULL,
    cpf character(11) NOT NULL,
    fk_codigo_turma character varying(10),
    CONSTRAINT aluno_cpf_check CHECK ((cpf ~ '[0,9]{11}'::text))
);


ALTER TABLE public.aluno OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16703)
-- Name: assiste; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.assiste (
    fk_horario_inicio_aula time without time zone,
    fk_matricula_aluno character varying
);


ALTER TABLE public.assiste OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16680)
-- Name: aula; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aula (
    horario_inicio_aula time without time zone NOT NULL,
    fk_id_professor integer,
    fk_numero_sala character varying(10)
);


ALTER TABLE public.aula OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16527)
-- Name: disciplina; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.disciplina (
    id_disciplina integer NOT NULL,
    nome character varying(20) NOT NULL,
    ementa character(10) NOT NULL
);


ALTER TABLE public.disciplina OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16525)
-- Name: disciplina_id_disciplina_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.disciplina_id_disciplina_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.disciplina_id_disciplina_seq OWNER TO postgres;

--
-- TOC entry 2995 (class 0 OID 0)
-- Dependencies: 198
-- Name: disciplina_id_disciplina_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.disciplina_id_disciplina_seq OWNED BY public.disciplina.id_disciplina;


--
-- TOC entry 200 (class 1259 OID 16538)
-- Name: leciona; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.leciona (
    fk_id_professor integer,
    fk_id_disciplina integer
);


ALTER TABLE public.leciona OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 16516)
-- Name: professor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.professor (
    id_professor integer NOT NULL,
    nome character varying(20) NOT NULL,
    cpf character(11) NOT NULL,
    matricula character varying(10),
    CONSTRAINT professor_cpf_check CHECK ((cpf ~ '[0,9]{11}'::text))
);


ALTER TABLE public.professor OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 16514)
-- Name: professor_id_professor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.professor_id_professor_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.professor_id_professor_seq OWNER TO postgres;

--
-- TOC entry 2996 (class 0 OID 0)
-- Dependencies: 196
-- Name: professor_id_professor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.professor_id_professor_seq OWNED BY public.professor.id_professor;


--
-- TOC entry 202 (class 1259 OID 16553)
-- Name: sala; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sala (
    id_sala integer NOT NULL,
    andar character(20) NOT NULL,
    numero_sala character varying(10) NOT NULL,
    complemento character varying(20) NOT NULL,
    capacidade character(30) NOT NULL
);


ALTER TABLE public.sala OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16551)
-- Name: sala_id_sala_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sala_id_sala_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sala_id_sala_seq OWNER TO postgres;

--
-- TOC entry 2997 (class 0 OID 0)
-- Dependencies: 201
-- Name: sala_id_sala_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sala_id_sala_seq OWNED BY public.sala.id_sala;


--
-- TOC entry 203 (class 1259 OID 16658)
-- Name: turma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.turma (
    codigo_turma character varying(10) NOT NULL,
    serie character varying(20) NOT NULL,
    qtd_alunos character(10) NOT NULL,
    semestre character varying(10) NOT NULL
);


ALTER TABLE public.turma OWNER TO postgres;

--
-- TOC entry 2823 (class 2604 OID 16530)
-- Name: disciplina id_disciplina; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.disciplina ALTER COLUMN id_disciplina SET DEFAULT nextval('public.disciplina_id_disciplina_seq'::regclass);


--
-- TOC entry 2821 (class 2604 OID 16519)
-- Name: professor id_professor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professor ALTER COLUMN id_professor SET DEFAULT nextval('public.professor_id_professor_seq'::regclass);


--
-- TOC entry 2824 (class 2604 OID 16556)
-- Name: sala id_sala; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sala ALTER COLUMN id_sala SET DEFAULT nextval('public.sala_id_sala_seq'::regclass);


--
-- TOC entry 2984 (class 0 OID 16663)
-- Dependencies: 204
-- Data for Name: aluno; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aluno (matricula_aluno, id_aluno, cr, nome, cpf, fk_codigo_turma) FROM stdin;
\.


--
-- TOC entry 2986 (class 0 OID 16703)
-- Dependencies: 206
-- Data for Name: assiste; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.assiste (fk_horario_inicio_aula, fk_matricula_aluno) FROM stdin;
\.


--
-- TOC entry 2985 (class 0 OID 16680)
-- Dependencies: 205
-- Data for Name: aula; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aula (horario_inicio_aula, fk_id_professor, fk_numero_sala) FROM stdin;
\.


--
-- TOC entry 2979 (class 0 OID 16527)
-- Dependencies: 199
-- Data for Name: disciplina; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.disciplina (id_disciplina, nome, ementa) FROM stdin;
\.


--
-- TOC entry 2980 (class 0 OID 16538)
-- Dependencies: 200
-- Data for Name: leciona; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.leciona (fk_id_professor, fk_id_disciplina) FROM stdin;
\.


--
-- TOC entry 2977 (class 0 OID 16516)
-- Dependencies: 197
-- Data for Name: professor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.professor (id_professor, nome, cpf, matricula) FROM stdin;
\.


--
-- TOC entry 2982 (class 0 OID 16553)
-- Dependencies: 202
-- Data for Name: sala; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sala (id_sala, andar, numero_sala, complemento, capacidade) FROM stdin;
\.


--
-- TOC entry 2983 (class 0 OID 16658)
-- Dependencies: 203
-- Data for Name: turma; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.turma (codigo_turma, serie, qtd_alunos, semestre) FROM stdin;
\.


--
-- TOC entry 2998 (class 0 OID 0)
-- Dependencies: 198
-- Name: disciplina_id_disciplina_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.disciplina_id_disciplina_seq', 1, false);


--
-- TOC entry 2999 (class 0 OID 0)
-- Dependencies: 196
-- Name: professor_id_professor_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.professor_id_professor_seq', 1, false);


--
-- TOC entry 3000 (class 0 OID 0)
-- Dependencies: 201
-- Name: sala_id_sala_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sala_id_sala_seq', 1, false);


--
-- TOC entry 2839 (class 2606 OID 16674)
-- Name: aluno aluno_cpf_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno
    ADD CONSTRAINT aluno_cpf_key UNIQUE (cpf);


--
-- TOC entry 2841 (class 2606 OID 16672)
-- Name: aluno aluno_cr_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno
    ADD CONSTRAINT aluno_cr_key UNIQUE (cr);


--
-- TOC entry 2843 (class 2606 OID 16670)
-- Name: aluno aluno_id_aluno_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno
    ADD CONSTRAINT aluno_id_aluno_key UNIQUE (id_aluno);


--
-- TOC entry 2845 (class 2606 OID 16668)
-- Name: aluno aluno_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno
    ADD CONSTRAINT aluno_pkey PRIMARY KEY (matricula_aluno);


--
-- TOC entry 2847 (class 2606 OID 16684)
-- Name: aula aula_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aula
    ADD CONSTRAINT aula_pkey PRIMARY KEY (horario_inicio_aula);


--
-- TOC entry 2831 (class 2606 OID 16534)
-- Name: disciplina disciplina_ementa_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.disciplina
    ADD CONSTRAINT disciplina_ementa_key UNIQUE (ementa);


--
-- TOC entry 2833 (class 2606 OID 16532)
-- Name: disciplina disciplina_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.disciplina
    ADD CONSTRAINT disciplina_pkey PRIMARY KEY (id_disciplina);


--
-- TOC entry 2827 (class 2606 OID 16524)
-- Name: professor professor_cpf_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professor
    ADD CONSTRAINT professor_cpf_key UNIQUE (cpf);


--
-- TOC entry 2829 (class 2606 OID 16522)
-- Name: professor professor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professor
    ADD CONSTRAINT professor_pkey PRIMARY KEY (id_professor);


--
-- TOC entry 2835 (class 2606 OID 16558)
-- Name: sala sala_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sala
    ADD CONSTRAINT sala_pkey PRIMARY KEY (numero_sala);


--
-- TOC entry 2837 (class 2606 OID 16662)
-- Name: turma turma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.turma
    ADD CONSTRAINT turma_pkey PRIMARY KEY (codigo_turma);


--
-- TOC entry 2850 (class 2606 OID 16675)
-- Name: aluno aluno_fk_codigo_turma_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno
    ADD CONSTRAINT aluno_fk_codigo_turma_fkey FOREIGN KEY (fk_codigo_turma) REFERENCES public.turma(codigo_turma);


--
-- TOC entry 2853 (class 2606 OID 16709)
-- Name: assiste assiste_fk_horario_inicio_aula_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assiste
    ADD CONSTRAINT assiste_fk_horario_inicio_aula_fkey FOREIGN KEY (fk_horario_inicio_aula) REFERENCES public.aula(horario_inicio_aula);


--
-- TOC entry 2854 (class 2606 OID 16714)
-- Name: assiste assiste_fk_matricula_aluno_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assiste
    ADD CONSTRAINT assiste_fk_matricula_aluno_fkey FOREIGN KEY (fk_matricula_aluno) REFERENCES public.aluno(matricula_aluno);


--
-- TOC entry 2851 (class 2606 OID 16685)
-- Name: aula aula_fk_id_professor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aula
    ADD CONSTRAINT aula_fk_id_professor_fkey FOREIGN KEY (fk_id_professor) REFERENCES public.professor(id_professor);


--
-- TOC entry 2852 (class 2606 OID 16690)
-- Name: aula aula_fk_numero_sala_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aula
    ADD CONSTRAINT aula_fk_numero_sala_fkey FOREIGN KEY (fk_numero_sala) REFERENCES public.sala(numero_sala);


--
-- TOC entry 2849 (class 2606 OID 16546)
-- Name: leciona leciona_fk_id_disciplina_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.leciona
    ADD CONSTRAINT leciona_fk_id_disciplina_fkey FOREIGN KEY (fk_id_disciplina) REFERENCES public.disciplina(id_disciplina);


--
-- TOC entry 2848 (class 2606 OID 16541)
-- Name: leciona leciona_fk_id_professor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.leciona
    ADD CONSTRAINT leciona_fk_id_professor_fkey FOREIGN KEY (fk_id_professor) REFERENCES public.professor(id_professor);


-- Completed on 2019-01-25 20:50:51 -02

--
-- PostgreSQL database dump complete
--

